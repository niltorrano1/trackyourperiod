FROM python:3
LABEL maintainer="niltorrano@gmail.com"

WORKDIR /

COPY src ./src
COPY templates ./templates
COPY requirements.txt ./

RUN pip install --no-cache-dir -r requirements.txt

CMD [ "python", "src/trackyourperiod.py" ]
