# Track Your Period

**Telegram Bot** for track the your period and save data safely using your personal Google Drive.
This will help you get to know yourself better using the **symptothermal method**.

Also, you can read/edit your period templates easily using Google Spreadsheets.

## Instructions

In order to use this application it is **necessary** to have a **Google account**. If you don't have one yet you can check the following link to [sign up](https://accounts.google.com/signup/v2/webcreateaccount?hl=en&flowName=GlifWebSignIn&flowEntry=SignUp).

### Configuration

Follow this steps to configure your Google account and create your own TrackYourPeriod telegram bot.

##### Telegram

1. Search **Telegram Bot** called [@BotFather](https://telegram.me/botfather). You already can find this bot searching in your Telegram contacts.
2. **Create** your Telegram Bot using `/newbot` command.
3. Follow the conversation with @BotFather until you're done. It is very important that you **keep the token** provided during creation process.
4. Once created you can customize other fields like the image, the description, etc. using the same @BotFather. Type "/" in your chat to see all commands.

##### Google Console (Grant access to Google APIs)

1. Go to [Google Console](https://console.cloud.google.com/projectcreate) and **create a new project**. The project name can be whatever you want.
2. Enable the following Google APIs: `Google Drive` `Google Sheets`
3. In credentials, create new **service account** and download credentials file in format **JSON**. Save this file in a safe place.
4. Also, copy **email address** assigned to service account and save for next step. Looks like something like that (example@....iam.gserviceaccount.com)

##### Google Drive

1. Go to [Google Drive](https://drive.google.com/) using your **personal account**.
2. **Create new folder** for periods. We recommends to create this folder in the root directory of your drive.
3. **Share** this folder with email related with service account saved in the previous step. **Service account must be folder editor**, being able to write.

### Installation / Deploy

1. **Clone** this repository and copy file `credentials/config.sample.py` and rename to `config.py`. 
*It is not really necessary to clone the entire repository, you can see this file and use it as a template to create the configuration file.*
2. **Put your credentials** into new file recently created using sample and save it. The Google Service Account credentials also must be provided in this file as *Python dict*.

### Run Bot
**Required**: [Docker Engine](https://docs.docker.com/)
Execute this command using console:
** YOUR_PATH must be replaced with an Absolute path to your `config.py` file.
```sh
$ docker run -d \
    --restart=always \
    -v YOUR_PATH/config.py:/credentials/config.py:ro \
    --name trackyourperiod \
    zohantorrano/trackyourperiod:latest
```

### RELEASES

* **0.0.1** 

### TODOs

* **Code refactor**. Reduce number of lines, repeated code and maybe use Python class for some functions.
* Any idea or proposal are welcome :)

### Python Libraries

| Name | Version |
| ------ | ------ |
| [pyTelegramBotApi](https://github.com/eternnoir/pyTelegramBotAPI) | 3.7.1 |
| [gspread](https://github.com/burnash/gspread) | 3.6.0 |
| [gspread-dataframe](https://github.com/robin900/gspread-dataframe) | 3.0.6 |
| [oauth2client](https://github.com/googleapis/oauth2client) | 4.1.3 |
| [google-api-python-client](https://github.com/googleapis/google-api-python-client) | 1.8.3 |
| [pandas](https://github.com/pandas-dev/pandas) | 1.0.3 |
| [matplotlib](https://github.com/matplotlib/matplotlib) | 3.2.1 |
| image | 1.5.32 |

### Contributors
* Nil Torrano <niltorrano@gmail.com>

### LICENSE

[GNU Library General Public License, version 2.0](https://www.gnu.org/licenses/old-licenses/lgpl-2.0.html)
