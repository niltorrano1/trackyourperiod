#!/usr/bin/python3
##################################################################################
#
#   *** Track Your Period - Telegram Bot ***
#
#   >> CONFIG SAMPLE <<
#
#   INSTRUCTIONS: Copy this file and rename to "config.py".
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#   Collaborators:
#       * Nil Torrano
#           <niltorrano@gmail.com>
#
#   May 2020
#
##################################################################################

# Telegram
BOT_TOKEN = "your_bot_token_here"  # Your Telegram bot token
TELEGRAM_ALLOWED_IDS = [123456789, 987654321]  # Add your Telegram user IDs here

# Google
DRIVE_ROOT_FOLDER = 'TrackYourPeriod'  # Root folder must be shared with service account
SHEET_BASENAME = 'TrackYourPeriod'  # Basename for period sheets. Example: TrackYourPeriod_YYYYMMDD_CURRENT
GOOGLE_ACCOUNT = 'example@gmail.com'  # Your personal Google account address
# Google service account - JSON with credentials
GOOGLE_SERVICE_ACCOUNT_CREDENTIALS = {
  "type": "",
  "project_id": "",
  "private_key_id": "",
  "private_key": "",
  "client_email": "...@....iam.gserviceaccount.com",
  "client_id": "",
  "auth_uri": "https://accounts.google.com/o/oauth2/auth",
  "token_uri": "https://oauth2.googleapis.com/token",
  "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
  "client_x509_cert_url": ""
}
