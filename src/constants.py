#!/usr/bin/python3
##################################################################################
#
#   *** Track Your Period - Telegram Bot ***
#
#   >> Track Your Period - Constants <<
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#   Collaborators:
#       * Nil Torrano
#           <niltorrano@gmail.com>
#
#   May 2020
#
##################################################################################

""" FIXED ROWS / COLUMNS - PERIOD TEMPLATE """
# First row/col = 0

DATES_ROW = 4
TEMP_ROW = 5
REG_DATA_ROW = 6
FEEL_ROW = 7
SWELLING_ROW = 8
PAIN_ROW = 9
BLOOD_ROW = 10
DISORDERS_ROW = 11
SEX_ROW = 12
SPORTS_ROW = 13
CAPRICE_ROW = 14
ALCOHOL_ROW = 15
NOTES_ROW = 16


""" TEMPLATE VALUES """

DATA_REGISTRY_OPS = {
    "menstruation": "M",
    "dry day without mucus": "S",
    "sticky mucus": "f",
    "wet mucus": "F",
    "peak day": "F+"
}

SEX_OPS = {
    "yes, with penetration": "P",
    "yes, genital contact": "S",
    "no": "N"
}
