#!/usr/bin/python3
##################################################################################
#
#   *** Track Your Period - Telegram Bot ***
#
#   >> GRAPHS GENERATOR <<
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#   Collaborators:
#       * Nil Torrano
#           <niltorrano@gmail.com>
#
#   May 2020
#
##################################################################################
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from PIL import Image
import matplotlib.dates as mdates
from io import BytesIO

import spreadsheets as sp


def generate_temp_graph(sheet_id, xaxis="cycle"):
    '''
    Return image graph in bytes format with temperature and days.

    Args:
        sheet_id: Google spreadsheet ID.
        xaxis: Select X axis. 'cycle' || 'date'
    '''

    df = sp.get_temperature_dataframe(sheet_id)

    # Compute first and last date
    first_date = df.iloc[df['cycle_days'].first_valid_index()-1]['date'].date()
    last_date = df.iloc[df['cycle_days'].last_valid_index()-1]['date'].date()

    if xaxis == "dates":
        plt.title(f"Temperature by date ({first_date.strftime('%Y-%m-%d')} - {last_date.strftime('%Y-%m-%d')})")
        plt.xlabel('Day Of Month')

        # Set x axis with dates range
        plt.xlim([first_date, last_date])
        plt.ylim([34.5, 38.5])

        # Insert data
        plt.plot(df['date'].to_list(), df['temperature'].to_list(), color="red")
    else:
        plt.title(f"Temperature by cycle day ({first_date.strftime('%Y-%m-%d')} - {last_date.strftime('%Y-%m-%d')})")
        plt.xlabel('Cycle Day')
        first_day = df.iloc[df['cycle_days'].first_valid_index()-1]['cycle_days']
        last_day = df.iloc[df['cycle_days'].last_valid_index()-1]['cycle_days']

        # Set x axis with cycle range
        plt.xlim([first_day, last_day])
        plt.ylim([34.5, 38.5])

        # Insert data
        plt.plot(df['cycle_days'].to_list(), df['temperature'].to_list(), color="red")

    # Set Y label, x axis format and grid
    plt.ylabel('Temperature')
    plt.gcf().autofmt_xdate()
    plt.grid(linestyle="--", color="b")
    # Export graph to bytes
    buf = BytesIO()
    plt.savefig(buf, format='png')  
    buf.seek(0)
    im = Image.open(buf)
    img_bytes = BytesIO()
    im.save(img_bytes, format='png')
    img_bytes = img_bytes.getvalue()

    # Clear plot figure
    plt.clf()
    plt.cla()
    plt.close()

    return img_bytes


def generate_datatable_image(sheet_id):
    '''
    Return table in image format with
    selected period template data.

    Args:
        sheet_id: Google spreadsheet ID to print data.
    '''

    df = sp.get_data_from_spreadsheet(sheet_id)
    df_col_header = df[0]

    start = 1
    end = 11
    fig, ax = plt.subplots(4, figsize=(16,5))
    for x in range(0, 4):
        # Hide axes and create table
        fig.patch.set_visible(False)
        ax[x].axis('off')
        ax[x].axis('tight')
        df_table = pd.concat([df_col_header, df.iloc[:,start:end]], axis=1)
        df_table = df_table.replace(np.nan, '', regex=True)
        datatable = ax[x].table(cellText=df_table.values, loc='center')
        datatable.auto_set_font_size(False)
        datatable.set_fontsize(10)
        fig.tight_layout()

        start += 10
        end += 10

    # Export graph to bytes
    buf = BytesIO()
    plt.savefig(buf, format='png')  
    buf.seek(0)
    im = Image.open(buf)
    img_bytes = BytesIO()
    im.save(img_bytes, format='png')
    img_bytes = img_bytes.getvalue()

    # Clear plot figure
    plt.clf()
    plt.cla()
    plt.close()

    return img_bytes
