#!/usr/bin/python3
##################################################################################
#
#   *** Track Your Period - Telegram Bot ***
#
#   >> Questions with OPTIONS asked to user <<
#
#   IN THIS FILE THERE IS ONLY QUESTIONS WITH OPTIONS (BUTTONS)!!!
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#   Collaborators:
#       * Nil Torrano
#           <niltorrano@gmail.com>
#
#   May 2020
#
##################################################################################

""" GENERIC """

yes_no_ops = ['Yes', 'No']


""" NEW PERIOD """

# Q: When will you take temparature?
temp_time_ops = ['5h - 6h', '6h - 7h', '7h - 8h', '8h - 9h', '9h - 10h',
                 '10h - 11h', '11h - 12h']
# Q: Which way you'll use to take temperature?
temp_way_options = ['Vaginal', 'Anal', 'Mouth']


""" INSERT NEW DATA """

# Q: Select option for data registry:
data_reg_ops = ['Menstruation', 'Dry day without mucus', 'Sticky mucus',
                'Wet mucus', 'Peak day']
# Q: Lova making or genital contact?
sex_ops = ['Yes, with penetration', 'Yes, genital contact', 'No']
