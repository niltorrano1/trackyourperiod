#!/usr/bin/python3
##################################################################################
#
#   *** Track Your Period - Telegram Bot ***
#
#   >> Google Spreadsheets <<
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#   Collaborators:
#       * Nil Torrano
#           <niltorrano@gmail.com>
#
#   May 2020
#
##################################################################################
import os
from datetime import datetime, timedelta
import sys
import constants as ct

import pandas as pd
import numpy as np

import gspread
from gspread_dataframe import get_as_dataframe, set_with_dataframe

from googleapiclient.discovery import build
from oauth2client.service_account import ServiceAccountCredentials

FILE_PATH = os.path.dirname(os.path.realpath(__file__))
sys.path.insert(1, FILE_PATH + '/../credentials/')
import config as cf


SCOPES = [
    'https://spreadsheets.google.com/feeds',
    'https://www.googleapis.com/auth/drive'
]

TEMPLATE_PATH = os.path.dirname(os.path.realpath(__file__)) + \
                '/../templates/TrackYourPeriod_Template.csv'


##### GOOGLE DRIVE & SPREADSHEETS METHODS #####

def get_credentials(service):
    '''
    Return opened session with selected service as parameter.

    Args:
        service: Google service to connect. 'gspread' | 'drive'
    '''

    creds = ServiceAccountCredentials.from_json_keyfile_dict(
            cf.GOOGLE_SERVICE_ACCOUNT_CREDENTIALS, SCOPES)

    if service == 'gspread':
        return gspread.authorize(creds)
    elif service == 'drive':
        return build('drive', 'v3', credentials=creds)
    else:
        raise('Error with Google credentials.')


### DRIVE ###

def get_root_folder_id(service):
    '''
    Return Google Drive root folder ID for
    TrackYourPeriod bot.

    Args:
        service: Google Drive opened session.
    '''

    # List folders owned by user
    q = "mimeType = 'application/vnd.google-apps.folder' and trashed=false"
    request = service.files().list(q=q,
                                   pageSize=100,
                                   fields='nextPageToken,' \
                                          'files(id, name),' \
                                          'incompleteSearch')

    files = []
    while request:
        response = request.execute()
        if response["incompleteSearch"]:
            raise Exception("Incomplete search returned")
        files += response["files"]
        request = service.files().list_next(request, response)

    root_folder = [f['id'] for f in files if f['name'] == cf.DRIVE_ROOT_FOLDER]
    if root_folder:
        return root_folder[0]
    else:
        raise Exception("No Google Drive folder found." \
                        "Please, create yourself and set configuration.")


def create_gdrive_folder(service, parent_id, name):
    '''
    Create G Drive folder.

    Args:
        service: Google Drive opened session.
        parent_id: Google Drive parent folder id.
        name: Folder name.
    '''

    body = {
        "name": name,
        "parents": [parent_id],
        "description": f"Folder with period template for year {name}",
        "mimeType": "application/vnd.google-apps.folder"
    }

    try:
        print(f"Creating new folder on G Drive with name => {name}")
        folder = service.files().create(body=body).execute()
    except Exception as exc:
        raise Exception("Could not create year folder in Google Drive.") from exc
    
    # Set permissions
    domain_permission = {
        'type': 'user',
        'role': 'owner',
        'emailAddress': [
            cf.GOOGLE_ACCOUNT, 
            cf.GOOGLE_SERVICE_ACCOUNT
        ]
    }
    service.permissions().create(
        fileId=folder['id'],
        body=domain_permission,
        fields="id",
        transferOwnership=True).execute()

    return folder['id']


def list_google_drive_folders(service, parent_id):
    '''
    Return list with Google Drive folders
    found into the provided folder.

    Args:
        service: Google Drive opened session.
        parent_id: Google Drive parent folder id.
    '''

    # List folders
    q = f"'{parent_id}' in parents and mimeType = 'application/vnd.google-apps.folder' and trashed=false"
    request = service.files().list(q=q,
                                   pageSize=100,
                                   fields='nextPageToken,' \
                                          'files(id, name),' \
                                          'incompleteSearch')

    files = []
    while request:
        response = request.execute()
        if response["incompleteSearch"]:
            raise Exception("Incomplete search returned")
        files += response["files"]
        request = service.files().list_next(request, response)

    return files


def get_year_folder_id(year=datetime.now().year):
    '''
    Return folder ID for year provided as parameter.

    Args:
        year: Folder year. Default = Current year
    '''

    service = get_credentials(service='drive')
    root_folder_id = get_root_folder_id(service)  # Get root folder ID
    folders = list_google_drive_folders(service, root_folder_id)
    
    year_folder = [f['id'] for f in folders if f['name'] == str(year)]
    # Create year folder if not exists
    if not year_folder:
        year_folder = create_gdrive_folder(service, root_folder_id, str(year))
    else:
        year_folder = year_folder[0]
    
    return year_folder


### SPREADSHEETS ###

def create_spreadsheet(service, title, folder_id):
    '''
    Create new empty spreadsheet. Share with 
    service account and move to project folder.

    Args:
        service: Google Drive session.
        title: Spreadsheet title.
        folder_id: Parent folder Drive ID.

    Return: New spreadsheet ID and web link.
    '''

    body = {
        'name': title,
        'mimeType': 'application/vnd.google-apps.spreadsheet',
        'parents': {
                    'kind': 'drive#fileLink',
                    'id': folder_id
                }
    }

    new_sheet = service.files().create(body=body).execute()
    sheet_id = new_sheet["id"]

    # Set permissions
    domain_permission = {
        'type': 'user',
        'role': 'owner',
        'emailAddress': [
            cf.GOOGLE_ACCOUNT, 
            cf.GOOGLE_SERVICE_ACCOUNT
        ]
    }
    service.permissions().create(
        fileId=sheet_id,
        body=domain_permission,
        fields="id",
        transferOwnership=True).execute()

    # Get current folder
    file = service.files().get(fileId=sheet_id,
                               fields='parents, webViewLink').execute()
    link = file['webViewLink']  # Save web link
    # Move the file to the new folder
    file = service.files().update(fileId=sheet_id,
                                  addParents=folder_id,
                                  fields='id').execute()

    return sheet_id, link


def list_spreadsheets(service):
    '''
    Return list with spreadsheets found
    in root project folder.

    Args:
        service: Google Drive session.
    '''

    service = get_credentials(service='drive')
    root_folder_id = get_root_folder_id(service)  # Get root folder ID
    folders = list_google_drive_folders(service, root_folder_id)

    files = []
    for parent in folders:
        q = "mimeType = 'application/vnd.google-apps.spreadsheet' and trashed=false" \
            "and '{parent_id}' in parents and trashed=false".format(parent_id=parent['id'])
        request = service.files().list(q=q,
                                    pageSize=100,
                                    fields='nextPageToken,' \
                                            'files(id, name),' \
                                            'incompleteSearch')

        while request:
            response = request.execute()
            if response["incompleteSearch"]:
                raise Exception("Incomplete search returned")
            files += response["files"]
            request = service.files().list_next(request, response)

    return files


def get_current_period():
    '''
    Return spreadsheet ID of opened period if exists.
    '''

    # Get spreadsheets list
    service_drive = get_credentials(service='drive')
    files = list_spreadsheets(service_drive)
    # Obtain the sheet_id to rename
    sheet_id = [f for f in files if "_CURRENT" in f["name"]]

    return sheet_id[0] if sheet_id else []


def rename_file(service, file_id, title):
    '''
    Rename drive file to title passed as param.

    Args:
        service: Google Drive session.
        folder_id: File Drive ID to rename.
        title: New title for file.
    '''

    body = {'name': title}
    service.files().update(fileId=file_id,
                           body=body).execute()


def create_period_template(date, data):
    '''
    Create new period template starting with
    date provided as param.

    Args:
        date: The date that will be used to start days count.
    '''

    # Load template
    df = pd.read_csv(TEMPLATE_PATH, sep=",", header=None, dtype=object)
        
    # Set numbers to column names
    df.columns = [i for i in range(len(df.columns))]

    service_drive = get_credentials(service='drive')
    folder_id = get_year_folder_id()  # Get current year folder ID
    # Set date to sheet name
    date = datetime.strptime(date, '%Y-%m-%d')
    title = "TrackYourPeriod_{}_CURRENT".format(date.strftime("%Y%m%d"))
    sheet_id, link = create_spreadsheet(service_drive, title, folder_id)
    
    service_gspread = get_credentials(service='gspread')
    sheet = service_gspread.open_by_key(sheet_id)
    # Load data to sheet
    df.at[1,5] = data['way_take_temp']
    df.at[2,5] = data['time_take_temp']
    df.at[1,1] = date.strftime('%Y')
    df.at[2,1] = date.strftime('%B')

    # Set dates for row "Date"
    col = 1  # Skip first column (0) with row title
    while col < len(df.columns):
        df.at[ct.DATES_ROW, col] = date.strftime("%Y-%m-%d")
        date += timedelta(days=1)
        col += 1
    # Remove column names
    new_header = df.iloc[0]
    df = df[1:]
    df.columns = new_header
    # Save dataframe to spreadsheet
    set_with_dataframe(sheet.sheet1, df)
    return title, link


def compute_cycle_length(sheet_id):
    '''
    Set period duration to cycle length field
    based with last valid temperature in template.

    Args:
        sheet_id: Google spreadsheet ID.
    '''

    service_gspread = get_credentials(service='gspread')
    sheet = service_gspread.open_by_key(sheet_id)
    df = get_as_dataframe(sheet.sheet1)
    # FIXME: 'get_as_dataframe' can not read first line.
    df_aux = pd.DataFrame(columns=df.columns, index=[1])
    df = df_aux.append(df, ignore_index=True)

    df.columns = [i for i in range(len(df.columns))]
    cycle_days = df.iloc[5].last_valid_index()
    df.at[2,3] = cycle_days
    # Remove column names
    new_header = df.iloc[0]
    df = df[1:]
    df.columns = new_header
    # Save dataframe to spreadsheet
    set_with_dataframe(sheet.sheet1, df)

    return cycle_days


def close_period_template():
    '''
    Close current period template renaming 
    file and removing CURRENT key.
    '''

    sheet = get_current_period()
    service_drive = get_credentials(service='drive')
    # Compute cycle days
    cycle_days = compute_cycle_length(sheet['id'])
    # Rename template
    new_name = sheet['name'].replace('CURRENT', datetime.now().strftime("%Y%m%d"))
    rename_file(service_drive, sheet['id'], new_name)

    return cycle_days


def date_is_valid_for_period(sheet_id, date):
    '''
    Check if date is available on period template.

    Args:
        sheet_id: Google spreadsheet ID.
        data: String with date in format 'YYYY-MM-DD'
    '''

    service_gspread = get_credentials(service='gspread')
    sheet = service_gspread.open_by_key(sheet_id)
    df = get_as_dataframe(sheet.sheet1)
    # FIXME: 'get_as_dataframe' can not read first line.
    df_aux = pd.DataFrame(columns=df.columns, index=[1])
    df = df_aux.append(df, ignore_index=True)

    df.columns = [i for i in range(len(df.columns))]
    # Search column for selected date
    df_dates = df.iloc[ct.DATES_ROW]
    if df_dates[df_dates == date].index.empty:
        return False
    else:
        return True


def insert_full_data_to_spreadsheet(sheet_id, data):
    '''
    Add new data recieved as parameter to
    specific sheet. The date is also in the 
    dictionary with the other data. This function
    is for update all data for specific day.

    Args:
        sheet_id: Google spreadsheet ID.
        data: dict with user data to add.
    '''

    service_gspread = get_credentials(service='gspread')
    sheet = service_gspread.open_by_key(sheet_id)
    df = get_as_dataframe(sheet.sheet1)
    # FIXME: 'get_as_dataframe' can not read first line.
    df_aux = pd.DataFrame(columns=df.columns, index=[1])
    df = df_aux.append(df, ignore_index=True)

    df.columns = [i for i in range(len(df.columns))]
    # Search column for selected date
    df_dates = df.iloc[ct.DATES_ROW]
    update_col = df_dates[df_dates == data['reg_date']].index[0]
    if update_col:
        # Load data to sheet in current column
        df.at[ct.TEMP_ROW,update_col] = data['temperature'] if data['temperature'] else 'R'
        df.at[ct.REG_DATA_ROW,update_col] = data['data_registry']
        df.at[ct.FEEL_ROW,update_col] = data['appearance_feeling']
        df.at[ct.SWELLING_ROW,update_col] = data['swelling']
        df.at[ct.PAIN_ROW,update_col] = data['pain']
        df.at[ct.BLOOD_ROW,update_col] = data['bleeding']
        df.at[ct.DISORDERS_ROW,update_col] = data['disorders']
        df.at[ct.SEX_ROW,update_col] = data['sex']
        df.at[ct.SPORTS_ROW,update_col] = data['sport']
        df.at[ct.CAPRICE_ROW,update_col] = data['caprice']
        df.at[ct.ALCOHOL_ROW,update_col] = data['alcohol']
        df.at[ct.NOTES_ROW,update_col] = data['notes']
        # Remove column names
        new_header = df.iloc[0]
        df = df[1:]
        df.columns = new_header
        # Save dataframe to spreadsheet
        set_with_dataframe(sheet.sheet1, df)
    
    return


def insert_data_to_spreadsheet(sheet_id, update_row, value, date):
    '''
    Add new data recieved as parameter to
    specific sheet. The date is also in the 
    dictionary with the other data. This function
    is for update selected field for specific day.

    Args:
        sheet_id: Google spreadsheet ID
        update_row: Template period row related with field to update
        value: Field value to insert
        date: Specific date (yyyy-mm-dd) related with selecte value
    '''

    service_gspread = get_credentials(service='gspread')
    sheet = service_gspread.open_by_key(sheet_id)
    df = get_as_dataframe(sheet.sheet1)
    # FIXME: 'get_as_dataframe' can not read first line.
    df_aux = pd.DataFrame(columns=df.columns, index=[1])
    df = df_aux.append(df, ignore_index=True)

    df.columns = [i for i in range(len(df.columns))]
    # Search column for selected date
    df_dates = df.iloc[ct.DATES_ROW]
    update_col = df_dates[df_dates == date].index[0]
    if update_col:
        df.at[update_row,update_col] = value
        # Remove column names
        new_header = df.iloc[0]
        df = df[1:]
        df.columns = new_header
        # Save dataframe to spreadsheet
        set_with_dataframe(sheet.sheet1, df)
    else:
        print("Insert cell not found. Template will not be modified.")

    return


def get_data_from_spreadsheet(sheet_id, date=None):
    '''
    Return information about specific date 
    in period template.

    Args:
        sheet_id: Google spreadsheet ID.
        data: String with date to retrieve (YYYY-MM-DD).
    '''

    data = {'result': {}}

    service_gspread = get_credentials(service='gspread')
    sheet = service_gspread.open_by_key(sheet_id)
    df = get_as_dataframe(sheet.sheet1)
    # FIXME: 'get_as_dataframe' can not read first line.
    df_aux = pd.DataFrame(columns=df.columns, index=[1])
    df = df_aux.append(df, ignore_index=True)
    df = df.replace(np.nan, '', regex=True)  # Set NaN values to empty string

    df.columns = [i for i in range(len(df.columns))]
    if date:
        # Search column for selected date
        df_dates = df.iloc[ct.DATES_ROW]
        read_col = df_dates[df_dates == date].index[0]
        if read_col:
            data['result']['temperature'] = df.iloc[ct.TEMP_ROW,read_col]
            data['result']['data_registry'] = df.iloc[ct.REG_DATA_ROW,read_col]
            data['result']['appearance_feeling'] = df.iloc[ct.FEEL_ROW,read_col]
            data['result']['swelling'] = df.iloc[ct.SWELLING_ROW,read_col]
            data['result']['pain'] = df.iloc[ct.PAIN_ROW,read_col]
            data['result']['bleeding'] = df.iloc[ct.BLOOD_ROW,read_col]
            data['result']['disorders'] = df.iloc[ct.DISORDERS_ROW,read_col]
            data['result']['sex'] = df.iloc[ct.SEX_ROW,read_col]
            data['result']['sport'] = df.iloc[ct.SPORTS_ROW,read_col]
            data['result']['caprice'] = df.iloc[ct.CAPRICE_ROW,read_col]
            data['result']['alcohol'] = df.iloc[ct.ALCOHOL_ROW,read_col]
            data['result']['notes'] = df.iloc[ct.NOTES_ROW,read_col]

        return data
    else:
        # Return full dataframe
        df = df[1:]
        df = df.replace('', np.nan, regex=True)
        return df[2:6]


def get_temperature_dataframe(sheet_id):
    '''
    Return dataframe with dates and temperatures
    ready for create graphs.

    Args:
        sheet_id: Google spreadsheet ID.
    '''

    service_gspread = get_credentials(service='gspread')
    sheet = service_gspread.open_by_key(sheet_id)
    df = get_as_dataframe(sheet.sheet1)
    # FIXME: 'get_as_dataframe' can not read first line.
    df_aux = pd.DataFrame(columns=df.columns, index=[1])
    df = df_aux.append(df, ignore_index=True)
    df = df.replace(np.nan, '', regex=True)  # Set NaN values to empty string

    df.columns = [i for i in range(len(df.columns))]
    df_res = pd.concat([df.iloc[4], df.iloc[5], df.iloc[3]], axis=1)
    df_res = df_res[1:]
    df_res.columns = ['date', 'temperature', 'cycle_days']
    df_res['temperature'] = pd.to_numeric(df_res['temperature'], errors='coerce')
    df_res['cycle_days'] = pd.to_numeric(df_res['cycle_days'], errors='coerce')
    df_res['date'] = pd.to_datetime(df_res['date'], format="%Y-%m-%d")

    return df_res
