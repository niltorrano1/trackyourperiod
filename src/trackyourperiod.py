#!/usr/bin/python3
##################################################################################
#
#   *** Track Your Period - Telegram Bot ***
#
#   >> MAIN <<
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#   Collaborators:
#       * Nil Torrano
#           <niltorrano@gmail.com>
#
#   May 2020
#
##################################################################################
import os
import sys
from datetime import datetime, timedelta
import constants as ct
import op_questions as opq
import spreadsheets
import graphs
import utils

import telebot
from telebot import types

import gspread
from oauth2client.service_account import ServiceAccountCredentials

# Import credentials
FILE_PATH = os.path.dirname(os.path.realpath(__file__))
sys.path.insert(1, FILE_PATH + '/../credentials/')
import config as cf


# Create TeleBot Instance
try:    
    bot = telebot.TeleBot(cf.BOT_TOKEN)
    print("Running bot...")
except Exception as e:
    print("Error starting Telegram bot: {e}".format(e))


def main():
    '''Run Telegram Bot'''

    print("Starting polling...")
    bot.polling()


##### METHODS #####

def check_permissions(user_id):
    '''
    Check if request user is allowed to use this bot.

    Args:
        user_id: Telegram User ID (9 digits)
    '''

    if user_id not in cf.TELEGRAM_ALLOWED_IDS: 
        return False
    
    return True


def get_forbidden_msg(username):
    '''
    Return text with forbidden message to
    users that are not allowd to use this bot.

    Args:
        username: Telegram username
    '''

    msg = "Hello {user},\n" \
          "You do not have permission to use this Bot.\n" \
          "If you want more information " \
          "visit <https://gitlab.com/niltorrano1/trackyourperiod>."

    return msg.format(user=username)


def get_help_msg():
    '''
    Return text with intructions to use this bot.
    '''

    msg  = ("** " + u"\U0001FA78" + " Track Your Period " + u"\U0001F5D2" + " - HELP **\n"
            "* /new_period => Create new period template.\n"
            "* /close_period => Close current period template.\n"
            "* /insert_full => Set or update all information for a selected day.\n"
            "* /insert_temp => Set or update temperature for a selected day.\n"
            "* /insert_data_reg => Set or update data registry for a selected day.\n"
            "* /insert_feel => Set or update appearance feeling for a selected day.\n"
            "* /insert_pain => Set or update breakthrough pain for a selected day.\n"
            "* /insert_bleeding => Set or update breakthrough bleeding for a selected day.\n"
            "* /insert_disorders => Set or update disorders for a selected day.\n"
            "* /insert_sex => Set or update sex information for a selected day.\n"
            "* /insert_sport => Set or update sports activity for a selected day.\n"
            "* /insert_caprice => Set or update if you have eaten any whim for a selected day.\n"
            "* /insert_alcohol => Set or update if you have drunk alcohol for a selected day.\n"
            "* /insert_notes => Set or update the notes for a selected day.\n"
            "* /get_info => Read the information available in period template for a selected date.\n"
            "* /get_full_info => Returns image with temperature and data registry by date for current period.\n"
            "* /get_graph_cycle => Returns a graph with temperatures by cycle day for current period.\n"
            "* /get_graph_dates => Returns a graph with temperatures by calendar date for current period.\n"
            "* /skip => Skip or EXIT from current activity/command.\n"
            "* /start => Starts conversation with bot.\n"
            "* /help => Print this commands list.\n\n"
            "** All commands that interact with a period are only available if there is an active period.\n\n"
            "+ INFO: https://gitlab.com/niltorrano1/trackyourperiod")

    return msg


def get_welcome_msg(username):
    '''
    Return text with welcome message.
    '''

    msg  = "Welcome {user}! \n" \
           "I will be happy to help you tracking your period " \
           "and get to know yourself better.\n" \
           "Write /help for information about what I can do."

    return msg.format(user=username)


def skip(message):
    '''
    Function to skip active process.
    '''

    remove_markup = types.ReplyKeyboardRemove(selective=False)
    bot.send_message(message.chat.id, "Skipped.", reply_markup=remove_markup)
    return


def ask_questions_new_template(message, template_date, q=0, answers={}):
    '''
    Asks the user for the required data 
    to create a new template.

    Args:
        message: Telebot message object
        template_date: Date when sheet starts.
        q: Number of question.
        answers: dict with answers.
    '''

    if '/skip' in message.text:
        skip(message)
        return

    if not q:
        q1_markup = utils.create_markup(opq.temp_way_options)
        bot.send_message(message.chat.id, "Which way you'll use to "\
                        "take temperature?", reply_markup=q1_markup)
        bot.register_next_step_handler(message, ask_questions_new_template,
                                       template_date, q=1)
        return
    
    if q == 1:
        # Save previous answer if value is allowed
        if message.text not in opq.temp_way_options:
            bot.send_message(message.chat.id, "Option not allowed."\
                             "Please select Vaginal, Anal or Mouth.")
            bot.register_next_step_handler(message, ask_questions_new_template,
                                           template_date)
            return
        remove_markup = types.ReplyKeyboardRemove(selective=False)
        bot.send_message(message.chat.id, "Perfect. {}".format(message.text), 
                         reply_markup=remove_markup)
        answers = {'way_take_temp': message.text}
        
        # Next question
        q2_markup = utils.create_markup(opq.temp_time_ops)
        bot.send_message(message.chat.id, "When will you "\
                        "take temparature?", reply_markup=q2_markup)
        bot.register_next_step_handler(message, ask_questions_new_template,
                                       template_date, q=2, answers=answers)
        return
    elif q == 2:
        # Save previous answer
        remove_markup = types.ReplyKeyboardRemove(selective=False)
        bot.send_message(message.chat.id, "Okey. {}".format(message.text), 
                         reply_markup=remove_markup)
        answers['time_take_temp'] = message.text

    # Create new period template in GDrive
    bot.send_message(message.chat.id, "That's all. Wait a moment until "\
                     "the template has been created...")
    sheet_name, link = spreadsheets.create_period_template(
                       date=template_date, data=answers)
    bot.send_message(message.chat.id, "Created successfully.\n" \
                     "Spreadsheet name: {name}".format(name=sheet_name))
    # Send web link
    bot.send_message(message.chat.id, u"\U0001F449" + ' ' + link)
    bot.send_message(message.chat.id, "Use /insert_full command to add new data " \
                    "or /help to view specific commands.")


def start_create_template_process(message, answers=[]):
    '''
    Create new period template forcing date 
    format to YYYY-MM-DD. If 'today' are sent
    it will use current date.

    Args:
        message: Telebot message object
        answers: List with user answers used for new template.
    '''

    # Restart if /new_period invoked
    if '/new_period' in message.text:
        new_period(message)
        return
    elif '/skip' in message.text:
        skip(message)
        return
    else:
        # Validate date format
        try:
            # Check if user are using today/yesterday flag
            if message.text.lower() == 'today':
                template_date = datetime.now().strftime("%Y-%m-%d")
            elif message.text.lower() == 'yesterday':
                template_date = (datetime.now() - timedelta(days=1)).strftime("%Y-%m-%d")
            else:
                datetime.strptime(message.text, '%Y-%m-%d')
                template_date = message.text
        except ValueError:
            message = bot.reply_to(message, "Date format must be YYYY-MM-DD.\n" \
                                   "Also you can use 'today' to set date as {today}.".format(
                                   today=datetime.now().strftime("%Y-%m-%d")))
            bot.register_next_step_handler(message, start_create_template_process)
            return

    bot.send_message(message.chat.id, "Checking if there is an active period... Please wait.")
    sheet = spreadsheets.get_current_period()
    if sheet:
        bot.send_message(message.chat.id, ("There is an active period ({sheet}).\n" \
                         "You must close an active period to start a new one.\n" \
                         "Use /close_period command to do it.".format(
                         sheet=sheet["name"])))
        skip(message)
        return

    bot.send_message(message.chat.id, "Creating period template starting "\
                     "from {date}...".format(date=template_date))
    bot.send_message(message.chat.id, "Please, answer the following "\
                    "questions to create the new template.")
    # Asks to the user some questions to create new template
    ask_questions_new_template(message, template_date)


def close_period_template(message):
    '''
    Close current period or skip if action
    is not confirmed by 'yes'.

    Args:
        message: Telebot message object
    '''

    if message.text.lower() == 'yes':
        bot.send_message(message.chat.id, "Closing period... Please wait.")
        cycle_days = spreadsheets.close_period_template()
        bot.send_message(message.chat.id, "Done. Total cycle days: {}".format(cycle_days))
    elif message.text.lower() == 'no':
        skip(message)
    elif '/skip' in message.text:
        skip(message)
        return
    else:
        bot.send_message(message.chat.id, "Please, confirm action or type 'no' to skip.\n")
        close_period(message)

def insert_full_data_template(message, sheet, q=0, answers={}):
    '''
    Insert new data into sheet passed as param.
    This function asks some questions to the 
    user to get data to insert.

    Args:
        message: Telebot message object
        sheet: Spreadsheet object with ID and name (minimum)
        q: Question number
        answers: Dict with user answers that will be added
                 period template.
    '''

    if '/skip' in message.text:
        skip(message)
        return

    if q == 0:
        # Validate selected date to insert and save in answers
        try:
            if message.text.lower() == 'today':
                reg_date = datetime.now().strftime("%Y-%m-%d")
            elif message.text.lower() == 'yesterday':
                reg_date = (datetime.now() - timedelta(days=1)).strftime("%Y-%m-%d")
            else:
                datetime.strptime(message.text, '%Y-%m-%d')
                reg_date = message.text
            
            # Check if provided date is available on current period
            if not spreadsheets.date_is_valid_for_period(sheet['id'], reg_date):
                bot.send_message(message.chat.id, 
                                "Date {} is not available in current period template. Please, insert valid date.".format(reg_date))
                bot.register_next_step_handler(message, insert_full_data_template, sheet)
                return    
            
            # Date is valid
            bot.send_message(message.chat.id, "Nice, {}".format(reg_date))
            answers = {'reg_date': reg_date}

            # Next question
            q1_markup = utils.create_markup(["Skip due menstruation"])
            bot.send_message(message.chat.id, "What temperature do you have?", reply_markup=q1_markup)
            bot.register_next_step_handler(message, insert_full_data_template, sheet,
                                           q=1, answers=answers)
            return
        except ValueError:
            message = bot.reply_to(message, "Date format must be YYYY-MM-DD.\n" \
                                    "Also you can use 'today' to set date as {today}.".format(
                                    today=datetime.now().strftime("%Y-%m-%d")))
            bot.register_next_step_handler(message, insert_full_data_template, sheet)
            return
    elif q == 1:
        try:
            # Previous answer can be skipped during menstruation.
            if 'menstruation' in message.text.lower():
                temperature = 0
            else:
                temperature = float(message.text)
            answers['temperature'] = temperature

            # Next question
            q2_markup = utils.create_markup(opq.data_reg_ops)
            bot.send_message(message.chat.id, "Select option for data registry:", 
                             reply_markup=q2_markup)
            bot.register_next_step_handler(message, insert_full_data_template, sheet,
                                           q=2, answers=answers)
            return
        except ValueError:
            message = bot.reply_to(message, "Temperature format must be 'XX.XX'.")
            bot.register_next_step_handler(message, insert_full_data_template, sheet,
                                           q=1, answers=answers)
            return
    elif q == 2:
        if message.text not in opq.data_reg_ops:
            bot.send_message(message.chat.id, "Option not allowed.")
            # Repeat question
            q2_markup = utils.create_markup(opq.data_reg_ops)
            bot.send_message(message.chat.id, "Select option for data registry:", 
                             reply_markup=q2_markup)
            bot.register_next_step_handler(message, insert_full_data_template, sheet,
                                           q=2, answers=answers)
            return
        
        # Save previous answer
        remove_markup = types.ReplyKeyboardRemove(selective=False)
        bot.send_message(message.chat.id, "Okay. {}".format(message.text), 
                         reply_markup=remove_markup)
        answers['data_registry'] = ct.DATA_REGISTRY_OPS[message.text.lower()]
        
        # Next question
        bot.send_message(message.chat.id, "Perfect, one last questions...")
        bot.send_message(message.chat.id, "Appearance feeling? Write 'no' if you don't want to answer.")
        bot.register_next_step_handler(message, insert_full_data_template, sheet,
                                       q=3, answers=answers)
        return
    elif q == 3:
        # Save previous
        answers['appearance_feeling'] = message.text

        # Next question
        q4_markup = utils.create_markup(opq.yes_no_ops)
        bot.send_message(message.chat.id, "Breast tenderness or swelling?", 
                            reply_markup=q4_markup)
        bot.register_next_step_handler(message, insert_full_data_template, sheet,
                                       q=4, answers=answers)
        return
    elif q == 4:
        if message.text not in opq.yes_no_ops:
            bot.send_message(message.chat.id, "Option not allowed.")
            # Reask question
            q4_markup = utils.create_markup(opq.yes_no_ops)
            bot.send_message(message.chat.id, "Breast tenderness or swelling?", 
                                reply_markup=q4_markup)
            bot.register_next_step_handler(message, insert_full_data_template, sheet,
                                           q=4, answers=answers)
            return
        
        # Save previous
        remove_markup = types.ReplyKeyboardRemove(selective=False)
        bot.send_message(message.chat.id, "Selected: {}".format(message.text), 
                         reply_markup=remove_markup)
        answers['swelling'] = message.text
        # Next question
        q5_markup = utils.create_markup(opq.yes_no_ops)
        bot.send_message(message.chat.id, "Breakthrough pain?", 
                         reply_markup=q5_markup)
        bot.register_next_step_handler(message, insert_full_data_template, sheet,
                                       q=5, answers=answers)
        return
    elif q == 5:
        if message.text not in opq.yes_no_ops:
            bot.send_message(message.chat.id, "Option not allowed.")
            # Reask question
            q5_markup = utils.create_markup(opq.yes_no_ops)
            bot.send_message(message.chat.id, "Breakthrough pain?", 
                            reply_markup=q5_markup)
            bot.register_next_step_handler(message, insert_full_data_template, sheet,
                                        q=5, answers=answers)
            return

        # Save previous
        remove_markup = types.ReplyKeyboardRemove(selective=False)
        bot.send_message(message.chat.id, "Selected: {}".format(message.text), 
                         reply_markup=remove_markup)
        answers['pain'] = message.text
        # Next question
        q6_markup = utils.create_markup(opq.yes_no_ops)
        bot.send_message(message.chat.id, "Breakthrough bleeding?", 
                         reply_markup=q6_markup)
        bot.register_next_step_handler(message, insert_full_data_template, sheet,
                                       q=6, answers=answers)
        return
    elif q == 6:
        if message.text not in opq.yes_no_ops:
            bot.send_message(message.chat.id, "Option not allowed.")
            # Reask question
            q6_markup = utils.create_markup(opq.yes_no_ops)
            bot.send_message(message.chat.id, "Breakthrough bleeding?", 
                            reply_markup=q6_markup)
            bot.register_next_step_handler(message, insert_full_data_template, sheet,
                                        q=6, answers=answers)
            return

        # Save previous
        remove_markup = types.ReplyKeyboardRemove(selective=False)
        bot.send_message(message.chat.id, "Selected: {}".format(message.text), 
                         reply_markup=remove_markup)
        answers['bleeding'] = message.text
        # Next question
        bot.send_message(message.chat.id, "Have you experienced any disorder?"\
                         " Write 'no' if you don't want to answer.")
        bot.register_next_step_handler(message, insert_full_data_template, sheet,
                                        q=7, answers=answers)
        return
    elif q == 7:
        # Previous answer
        answers['disorders'] = message.text

        # Next question
        q7_markup = utils.create_markup(opq.sex_ops)
        bot.send_message(message.chat.id, "Love making or genital contact?", 
                         reply_markup=q7_markup)
        bot.register_next_step_handler(message, insert_full_data_template, sheet,
                                       q=8, answers=answers)
        return
    elif q == 8:
        if message.text not in opq.sex_ops:
            bot.send_message(message.chat.id, "Option not allowed.")
            # Reask question
            q7_markup = utils.create_markup(opq.sex_ops)
            bot.send_message(message.chat.id, "Love making or genital contact?", 
                            reply_markup=q7_markup)
            bot.register_next_step_handler(message, insert_full_data_template, sheet,
                                        q=8, answers=answers)
            return

        # Save previous answer
        remove_markup = types.ReplyKeyboardRemove(selective=False)
        bot.send_message(message.chat.id, "Selected: {}".format(message.text), 
                         reply_markup=remove_markup)
        answers['sex'] = ct.SEX_OPS[message.text.lower()]

        # Next question
        q8_markup = utils.create_markup(opq.yes_no_ops)
        bot.send_message(message.chat.id, "Have you did sports?",
                         reply_markup=q8_markup)
        bot.register_next_step_handler(message, insert_full_data_template, sheet,
                                       q=9, answers=answers)
        return
    elif q == 9:
        if message.text not in opq.yes_no_ops:
            bot.send_message(message.chat.id, "Option not allowed.")
            # Reask question
            q8_markup = utils.create_markup(opq.yes_no_ops)
            bot.send_message(message.chat.id, "Have you did sports?",
                            reply_markup=q8_markup)
            bot.register_next_step_handler(message, insert_full_data_template, sheet,
                                        q=9, answers=answers)
            return

        # Save previous answer
        remove_markup = types.ReplyKeyboardRemove(selective=False)
        bot.send_message(message.chat.id, "Selected: {}".format(message.text), 
                         reply_markup=remove_markup)
        answers['sport'] = message.text
        
        # Next question
        q9_markup = utils.create_markup(opq.yes_no_ops)
        bot.send_message(message.chat.id, "Have you indulged yourself to eat?",
                         reply_markup=q9_markup)
        bot.register_next_step_handler(message, insert_full_data_template, sheet,
                                       q=10, answers=answers)
        return
    elif q == 10:
        if message.text not in opq.yes_no_ops:
            bot.send_message(message.chat.id, "Option not allowed.")
            # Reask question
            q9_markup = utils.create_markup(opq.yes_no_ops)
            bot.send_message(message.chat.id, "Have you indulged yourself to eat?",
                            reply_markup=q9_markup)
            bot.register_next_step_handler(message, insert_full_data_template, sheet,
                                           q=10, answers=answers)
            return

        # Save previous answer
        remove_markup = types.ReplyKeyboardRemove(selective=False)
        bot.send_message(message.chat.id, "Selected: {}".format(message.text), 
                         reply_markup=remove_markup)
        answers['caprice'] = message.text

        # Next question
        q10_markup = utils.create_markup(opq.yes_no_ops)
        bot.send_message(message.chat.id, "Have you drunk alcohol?",
                         reply_markup=q10_markup)
        bot.register_next_step_handler(message, insert_full_data_template, sheet,
                                       q=11, answers=answers)
        return
    elif q == 11:
        if message.text not in opq.yes_no_ops:
            bot.send_message(message.chat.id, "Option not allowed.")
            # Reask question
            q10_markup = utils.create_markup(opq.yes_no_ops)
            bot.send_message(message.chat.id, "Have you drunk alcohol?",
                            reply_markup=q10_markup)
            bot.register_next_step_handler(message, insert_full_data_template, sheet,
                                           q=11, answers=answers)
            return

        # Save previous answer
        remove_markup = types.ReplyKeyboardRemove(selective=False)
        bot.send_message(message.chat.id, "Selected: {}".format(message.text), 
                         reply_markup=remove_markup)
        answers['alcohol'] = message.text

        # Next question
        bot.send_message(message.chat.id, "Do you want to add extra notes?")
        bot.register_next_step_handler(message, insert_full_data_template, sheet,
                                       q=12, answers=answers)
        return
    elif q == 12:
        answers['notes'] = message.text
    
    # Add new row with collected data
    bot.send_message(message.chat.id, "Great, let's add this data to the period tracker. "\
                    "Wait a moment please...")
    spreadsheets.insert_full_data_to_spreadsheet(sheet['id'], answers)
    bot.send_message(message.chat.id, f"Data updated successfully for the date {answers['reg_date']}!")


def validate_insert_data_template(message, sheet, action, date):
    '''
    Validate value selected to insert into period template.

    Args:
        message: Telebot message object with value
        sheet: Spreadsheet object with ID and name (minimum)
        action: Selected field to update
        date: Selected date to set field
    '''

    # Skip inserting actions
    if '/skip' in message.text:
        skip(message)
        return
    elif action == '/skip':
        skip(message)
        return

    if action == "/insert_temp":
        try:
            temperature = float(message.text)
        except ValueError:
            message = bot.reply_to(message, "Temperature format must be 'XX.XX'.")
            insert_data_template(message, sheet, action, date)
            return
        
        bot.send_message(message.chat.id, "Inserting data... Please wait.")
        spreadsheets.insert_data_to_spreadsheet(sheet['id'], ct.TEMP_ROW, 
                                                temperature, date)
        bot.send_message(message.chat.id, f"Data saved successfully!\nDate: {date}\nTemp: {temperature}")
        return
    elif action == "/insert_data_reg":
        if message.text not in opq.data_reg_ops:
            bot.send_message(message.chat.id, "Option not allowed.")
            insert_data_template(message, sheet, action, date)
            return
        
        remove_markup = types.ReplyKeyboardRemove(selective=False)
        bot.send_message(message.chat.id, "Inserting data... Please wait.", reply_markup=remove_markup)
        spreadsheets.insert_data_to_spreadsheet(sheet['id'], ct.REG_DATA_ROW, 
                                                ct.DATA_REGISTRY_OPS[message.text.lower()], date)
        bot.send_message(message.chat.id, f"Data saved successfully!\nDate: {date}\nData Registry: {message.text}")
    elif action == "/insert_feel":
        bot.send_message(message.chat.id, "Inserting data... Please wait.")
        spreadsheets.insert_data_to_spreadsheet(sheet['id'], ct.FEEL_ROW, 
                                                message.text, date)
        bot.send_message(message.chat.id, f"Data saved successfully!\nDate: {date}\nAppearance feeling: {message.text}")
    elif action == "/insert_swelling":
        if message.text not in opq.yes_no_ops:
            bot.send_message(message.chat.id, "Option not allowed.")
            insert_data_template(message, sheet, action, date)
            return

        remove_markup = types.ReplyKeyboardRemove(selective=False)
        bot.send_message(message.chat.id, "Inserting data... Please wait.", reply_markup=remove_markup)
        spreadsheets.insert_data_to_spreadsheet(sheet['id'], ct.SWELLING_ROW, 
                                                message.text, date)
        bot.send_message(message.chat.id, f"Data saved successfully!\nDate: {date}\n"
                         f"Swelling or breast tenderness: {message.text}")
    elif action == "/insert_pain":
        if message.text not in opq.yes_no_ops:
            bot.send_message(message.chat.id, "Option not allowed.")
            insert_data_template(message, sheet, action, date)
            return

        remove_markup = types.ReplyKeyboardRemove(selective=False)
        bot.send_message(message.chat.id, "Inserting data... Please wait.", reply_markup=remove_markup)
        spreadsheets.insert_data_to_spreadsheet(sheet['id'], ct.PAIN_ROW, 
                                                message.text, date)
        bot.send_message(message.chat.id, f"Data saved successfully!\nDate: {date}\n"
                         f"Breakthrough pain: {message.text}")
    elif action == "/insert_bleeding":
        if message.text not in opq.yes_no_ops:
            bot.send_message(message.chat.id, "Option not allowed.")
            insert_data_template(message, sheet, action, date)
            return

        remove_markup = types.ReplyKeyboardRemove(selective=False)
        bot.send_message(message.chat.id, "Inserting data... Please wait.", reply_markup=remove_markup)
        spreadsheets.insert_data_to_spreadsheet(sheet['id'], ct.BLOOD_ROW, 
                                                message.text, date)
        bot.send_message(message.chat.id, f"Data saved successfully!\nDate: {date}\n"
                         f"Breakthrough bleeding: {message.text}")
    elif action == "/insert_disorders":
        bot.send_message(message.chat.id, "Inserting data... Please wait.")
        spreadsheets.insert_data_to_spreadsheet(sheet['id'], ct.DISORDERS_ROW, 
                                                message.text, date)
        bot.send_message(message.chat.id, f"Data saved successfully!\nDate: {date}\nDisorders: {message.text}")
    elif action == "/insert_sex":
        if message.text not in opq.sex_ops:
            bot.send_message(message.chat.id, "Option not allowed.")
            insert_data_template(message, sheet, action, date)
            return

        remove_markup = types.ReplyKeyboardRemove(selective=False)
        bot.send_message(message.chat.id, "Inserting data... Please wait.", reply_markup=remove_markup)
        spreadsheets.insert_data_to_spreadsheet(sheet['id'], ct.SEX_ROW, 
                                                ct.SEX_OPS[message.text.lower()], date)
        bot.send_message(message.chat.id, f"Data saved successfully!\nDate: {date}\n"
                         f"Make love or genital contact: {message.text}")
    elif action == "/insert_sport":
        if message.text not in opq.yes_no_ops:
            bot.send_message(message.chat.id, "Option not allowed.")
            insert_data_template(message, sheet, action, date)
            return

        remove_markup = types.ReplyKeyboardRemove(selective=False)
        bot.send_message(message.chat.id, "Inserting data... Please wait.", reply_markup=remove_markup)
        spreadsheets.insert_data_to_spreadsheet(sheet['id'], ct.SPORTS_ROW, 
                                                message.text, date)
        bot.send_message(message.chat.id, f"Data saved successfully!\nDate: {date}\n"
                         f"Sport: {message.text}")
    elif action == "/insert_caprice":
        if message.text not in opq.yes_no_ops:
            bot.send_message(message.chat.id, "Option not allowed.")
            insert_data_template(message, sheet, action, date)
            return

        remove_markup = types.ReplyKeyboardRemove(selective=False)
        bot.send_message(message.chat.id, "Inserting data... Please wait.", reply_markup=remove_markup)
        spreadsheets.insert_data_to_spreadsheet(sheet['id'], ct.CAPRICE_ROW, 
                                                message.text, date)
        bot.send_message(message.chat.id, f"Data saved successfully!\nDate: {date}\n"
                         f"Caprice: {message.text}")
    elif action == "/insert_alcohol":
        if message.text not in opq.yes_no_ops:
            bot.send_message(message.chat.id, "Option not allowed.")
            insert_data_template(message, sheet, action, date)
            return

        remove_markup = types.ReplyKeyboardRemove(selective=False)
        bot.send_message(message.chat.id, "Inserting data... Please wait.", reply_markup=remove_markup)
        spreadsheets.insert_data_to_spreadsheet(sheet['id'], ct.ALCOHOL_ROW, 
                                                message.text, date)
        bot.send_message(message.chat.id, f"Data saved successfully!\nDate: {date}\n"
                         f"Alcohol: {message.text}")
    elif action == "/insert_notes":
        bot.send_message(message.chat.id, "Inserting data... Please wait.")
        spreadsheets.insert_data_to_spreadsheet(sheet['id'], ct.NOTES_ROW, 
                                                message.text, date)
        bot.send_message(message.chat.id, f"Data saved successfully!\nDate: {date}\nNotes: {message.text}")
    else:
        print(f"Uknown action ({action}) to validate.")

    return


def insert_data_template(message, sheet, action, date=None, q=1):
    '''
    Insert new data into period template passed as param.
    This function ask value for the selected field
    to insert.

    Args:
        message: Telebot message object
        sheet: Spreadsheet object with ID and name (minimum)
        action: Selected field to update
        date: Selected date to set field
    '''

    # Skip inserting actions
    if '/skip' in message.text:
        skip(message)
        return
    elif action == '/skip':
        skip(message)
        return
    
    if not date:
        if not q:
            bot.send_message(message.chat.id, "What date do you want to update?\n"
                            "You can type 'today', 'yesterday' or specific date with format YYYY-MM-DD.")
            bot.register_next_step_handler(message, insert_data_template, sheet, action)
            return
        
        # Validate selected date to insert new data
        try:
            if message.text.lower() == 'today':
                date = datetime.now().strftime("%Y-%m-%d")
            elif message.text.lower() == 'yesterday':
                date = (datetime.now() - timedelta(days=1)).strftime("%Y-%m-%d")
            else:
                datetime.strptime(message.text, '%Y-%m-%d')
                date = message.text
            
            # Check if provided date is available on current period
            if not spreadsheets.date_is_valid_for_period(sheet['id'], date):
                bot.send_message(message.chat.id, 
                                f"Date {date} is not available in current period template. Please, insert valid date.")
                bot.register_next_step_handler(message, insert_data_template, sheet, action)
                return
            # Date is valid
            bot.send_message(message.chat.id, f"Nice, {date}")
        except ValueError:
            message = bot.reply_to(message, "Date format must be YYYY-MM-DD.\n" \
                                    "Also you can use 'today' to set date as {today}.".format(
                                    today=datetime.now().strftime("%Y-%m-%d")))
            bot.register_next_step_handler(message, insert_data_template, sheet, action)
            return 

    # Send message with question associated with selected action
    if action == "/insert_temp":
        bot.send_message(message.chat.id, "What temperature do you have?")
        bot.register_next_step_handler(message, validate_insert_data_template, sheet,
                                       action, date)
        return
    elif action == "/insert_data_reg":
        markup = utils.create_markup(opq.data_reg_ops)
        bot.send_message(message.chat.id, "Select option for data registry:", 
                            reply_markup=markup)
        bot.register_next_step_handler(message, validate_insert_data_template, sheet,
                                       action, date)
        return
    elif action == "/insert_feel":
        bot.send_message(message.chat.id, "What do you want to report about appearance feeling?")
        bot.register_next_step_handler(message, validate_insert_data_template, sheet,
                                       action, date)
    elif action == "/insert_swelling":
        markup = utils.create_markup(opq.yes_no_ops)
        bot.send_message(message.chat.id, "Do you have breast tenderness or swelling?", 
                         reply_markup=markup)
        bot.register_next_step_handler(message, validate_insert_data_template, sheet,
                                       action, date)
    elif action == "/insert_pain":
        markup = utils.create_markup(opq.yes_no_ops)
        bot.send_message(message.chat.id, "Do you have breakthrough pain?", 
                         reply_markup=markup)
        bot.register_next_step_handler(message, validate_insert_data_template, sheet,
                                       action, date)
    elif action == "/insert_bleeding":
        markup = utils.create_markup(opq.yes_no_ops)
        bot.send_message(message.chat.id, "Do you have breakthrough bleeding?", 
                         reply_markup=markup)
        bot.register_next_step_handler(message, validate_insert_data_template, sheet,
                                       action, date)
    elif action == "/insert_disorders":
        bot.send_message(message.chat.id, "Do you have any disorders?")
        bot.register_next_step_handler(message, validate_insert_data_template, sheet,
                                       action, date)
    elif action == "/insert_sex":
        markup = utils.create_markup(opq.sex_ops)
        bot.send_message(message.chat.id, "Have you made love or genital contact?", 
                         reply_markup=markup)
        bot.register_next_step_handler(message, validate_insert_data_template, sheet,
                                       action, date)
    elif action == "/insert_sport":
        markup = utils.create_markup(opq.yes_no_ops)
        bot.send_message(message.chat.id, "Have you played sports?", 
                         reply_markup=markup)
        bot.register_next_step_handler(message, validate_insert_data_template, sheet,
                                       action, date)
    elif action == "/insert_caprice":
        markup = utils.create_markup(opq.yes_no_ops)
        bot.send_message(message.chat.id, "Have you indulged yourself to eat?", 
                         reply_markup=markup)
        bot.register_next_step_handler(message, validate_insert_data_template, sheet,
                                       action, date)
    elif action == "/insert_alcohol":
        markup = utils.create_markup(opq.yes_no_ops)
        bot.send_message(message.chat.id, "Have you drunk alcohol?", 
                         reply_markup=markup)
        bot.register_next_step_handler(message, validate_insert_data_template, sheet,
                                       action, date)
    else:
        bot.send_message(message.chat.id, f"Action ({action}) not found.\nType /help for more information.")
        print(f"An error has occurred with this action ({action})")
        return


def fetch_info_from_current_template(message, sheet):
    '''
    Return information available in period template
    provided in function params.

    Args:
        message: Telebot message object
        sheet: Spreadsheet object with ID and name (minimum)
    '''

    # Skip inserting actions
    if '/skip' in message.text:
        skip(message)
        return

    try:
        if message.text.lower() == 'today':
            date = datetime.now().strftime("%Y-%m-%d")
        elif message.text.lower() == 'yesterday':
            date = (datetime.now() - timedelta(days=1)).strftime("%Y-%m-%d")
        else:
            datetime.strptime(message.text, '%Y-%m-%d')
            date = message.text
        
        # Check if provided date is available on current period
        if not spreadsheets.date_is_valid_for_period(sheet['id'], date):
            bot.send_message(message.chat.id, 
                            "Date {} is not available in current period template. Please, insert valid date.".format(date))
            bot.register_next_step_handler(message, fetch_info_from_current_template, sheet)
            return    
        
        # Date is valid
        data = spreadsheets.get_data_from_spreadsheet(sheet['id'], date)
        # Check if all data are empty or not
        if [data['result'][x] for x in data['result'] if data['result'][x] != '']:
            msg = f"Showing data regarding the date {date}:\n"
            msg += ">> " + u"\U0001F321" + f" {data['result']['temperature']}\n"
            msg += ">> Data Registry:" + f" {data['result']['data_registry']}\n"
            msg += ">> Apperance/Feeling:" + f" {data['result']['appearance_feeling']}\n"
            msg += ">> Swelling:" + f" {data['result']['swelling']}\n"
            msg += ">> Pain:" + f" {data['result']['pain']}\n"
            msg += ">> " + u"\U0001FA78" + f" {data['result']['bleeding']}\n"
            msg += ">> Disorders:" + f" {data['result']['disorders']}\n"
            msg += ">> Sex:" + f" {data['result']['sex']}\n"
            msg += ">> " + u"\U0001F5D2" + f" {data['result']['notes']}\n"
            bot.send_message(message.chat.id, msg)
        else:
            bot.send_message(message.chat.id, "No data available for selected day.")

        return
    except ValueError:
            message = bot.reply_to(message, "Date format must be YYYY-MM-DD.\n" \
                                    "Also you can use 'today' to set date as {today}.".format(
                                    today=datetime.now().strftime("%Y-%m-%d")))
            bot.register_next_step_handler(message, fetch_info_from_current_template, sheet)
            return

##### MESSAGE HANDLERS #####

@bot.message_handler(commands=['help'])
def send_help(message):
    if not check_permissions(message.from_user.id):
        bot.reply_to(message, get_forbidden_msg(message.from_user.username))
        return

    bot.reply_to(message, get_help_msg())


@bot.message_handler(commands=['start'])
def send_welcome(message):
    if not check_permissions(message.from_user.id):
        bot.reply_to(message, get_forbidden_msg(message.from_user.username))
        return
    
    bot.reply_to(message, get_welcome_msg(message.from_user.username))


@bot.message_handler(commands=['new_period'])
def new_period(message):
    if not check_permissions(message.from_user.id):
        bot.reply_to(message, get_forbidden_msg(message.from_user.username))
        return

    bot.send_message(message.chat.id, "Okey, let's create a new period template!\n" \
                                      "First of all...\n")
    question = "What date do you want to use as the first day?\n"\
               "You can send 'today' to set date to {today}.".format(
                today=datetime.now().strftime("%Y-%m-%d"))

    bot.send_message(message.chat.id, question)
    bot.register_next_step_handler(message, start_create_template_process)


@bot.message_handler(commands=['close_period'])
def close_period(message):
    if not check_permissions(message.from_user.id):
        bot.reply_to(message, get_forbidden_msg(message.from_user.username))
        return

    bot.send_message(message.chat.id, "Checking if there is an active period... Please wait.")
    sheet = spreadsheets.get_current_period()
    if sheet:
        bot.send_message(message.chat.id, ("Are you sure you want " \
                         "to close this period ({sheet})?\n" \
                         "Type 'yes' to confirm or /skip to exit.").format(sheet=sheet["name"]))
        bot.register_next_step_handler(message, close_period_template)
    else:
        bot.send_message(message.chat.id, "No active periods found.\n" \
                         "You can create using /new_period command.")


@bot.message_handler(commands=['insert_full'])
def insert_full_command(message):
    if not check_permissions(message.from_user.id):
        bot.reply_to(message, get_forbidden_msg(message.from_user.username))
        return

    bot.send_message(message.chat.id, "Inserting new data.\nFirst of all, " \
                    "let's check that there is an active period. Please wait...")
    sheet = spreadsheets.get_current_period()
    if sheet:
        bot.send_message(message.chat.id, ("Found active period: {sheet}").format(
                         sheet=sheet["name"]))
        bot.send_message(message.chat.id, "What date do you want to add? "\
                        "You can send 'today' or specific date.")
        bot.register_next_step_handler(message, insert_full_data_template, sheet)
    else:
        bot.send_message(message.chat.id, "No active periods found.\n" \
                         "You can create using /new_period command.")


@bot.message_handler(commands=[
    "insert_temp", "insert_data_reg", "insert_feel", "insert_swelling",
    "insert_pain", "insert_bleeding", "insert_disorders", "insert_sex",
    "insert_sport", "insert_caprice", "insert_alcohol", "insert_notes"
])
def insert_command(message):
    if not check_permissions(message.from_user.id):
        bot.reply_to(message, get_forbidden_msg(message.from_user.username))
        return
    
    bot.send_message(message.chat.id, f"Inserting data.\nFirst of all, " \
                    "let's check that there is an active period. Please wait...")
    sheet = spreadsheets.get_current_period()
    if sheet:
        bot.send_message(message.chat.id, ("Found active period: {sheet}").format(
                         sheet=sheet["name"]))
        insert_data_template(message, sheet, message.text, date=None, q=0)
    else:
        bot.send_message(message.chat.id, "No active periods found.\n" \
                         "You can create using /new_period command.")
    

@bot.message_handler(commands=["get_info"])
def get_info(message):
    if not check_permissions(message.from_user.id):
        bot.reply_to(message, get_forbidden_msg(message.from_user.username))
        return
    
    sheet = spreadsheets.get_current_period()
    if sheet:
        bot.send_message(message.chat.id, "What date would you like to see?\n" \
                    "Keep in mind that only current period can be read.")
        bot.register_next_step_handler(message, fetch_info_from_current_template, sheet)
    else:
        bot.send_message(message.chat.id, "No active periods found.\n" \
                         "You can create using /new_period command.")


@bot.message_handler(commands=["get_full_info"])
def get_full_info(message):
    if not check_permissions(message.from_user.id):
        bot.reply_to(message, get_forbidden_msg(message.from_user.username))
        return
    
    sheet = spreadsheets.get_current_period()
    if sheet:
        img_bytes = graphs.generate_datatable_image(sheet["id"])
        bot.send_photo(message.chat.id, img_bytes)
        # bot.send_message(message.chat.id, "What date would you like to see?\n" \
        #             "Keep in mind that only current period can be read.")
        # bot.register_next_step_handler(message, fetch_info_from_current_template, sheet)
    else:
        bot.send_message(message.chat.id, "No active periods found.\n" \
                         "You can create using /new_period command.")


@bot.message_handler(commands=['get_graph_dates', 'get_graph_cycle'])
def get_graphs(message):
    if not check_permissions(message.from_user.id):
        bot.reply_to(message, get_forbidden_msg(message.from_user.username))
        return

    bot.send_message(message.chat.id, "Printing graph.\nFirst of all, " \
                    "let's check that there is an active period. Please wait...")
    sheet = spreadsheets.get_current_period()
    if sheet:
        bot.send_message(message.chat.id, ("Found active period: {sheet}").format(
                         sheet=sheet["name"]))
        xaxis = 'cycle' if 'cycle' in message.text else 'dates'
        bot.send_message(message.chat.id, "Wait until graph is ready...")
        img_bytes = graphs.generate_temp_graph(sheet['id'], xaxis=xaxis)
        bot.send_photo(message.chat.id, img_bytes)
    else:
        bot.send_message(message.chat.id, "No active periods found.\n" \
                         "You can create using /new_period command.")


@bot.message_handler(commands=['skip'])
def skip_command(message):
    if not check_permissions(message.from_user.id):
        bot.reply_to(message, get_forbidden_msg(message.from_user.username))
        return

    # Call skip method. Dummy
    skip(message)


@bot.message_handler(func=lambda message: True)
def echo_all(message):
    bot.send_message(message.chat.id, "Command not found.")
    bot.reply_to(message, get_help_msg())


if __name__ == '__main__':
    main()
