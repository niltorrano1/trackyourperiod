#!/usr/bin/python3
##################################################################################
#
#   *** Track Your Period - Telegram Bot ***
#
#   >> Utils <<
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#   Collaborators:
#       * Nil Torrano
#           <niltorrano@gmail.com>
#
#   May 2020
#
##################################################################################
from telebot import types


def create_markup(options):
    '''
    Return Telegram markup object with options
    provided as parameter.
    Keep in mind that will create as many buttons
    as options.

    Args:
        options: List with markup options.
    '''

    markup = types.ReplyKeyboardMarkup()
    for op in options: 
        button = types.KeyboardButton(op)
        markup.add(button)
    
    return markup
