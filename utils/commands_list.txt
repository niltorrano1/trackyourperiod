new_period - Create new period template.
close_period - Close current period template.
insert_full - Set or update all information for a selected day.
insert_temp - Set or update temperature for a selected day.
insert_data_reg - Set or update data registry for a selected day.
insert_feel - Set or update appearance feeling for a selected day.
insert_pain - Set or update breakthrough pain for a selected day.
insert_bleeding - Set or update breakthrough bleeding for a selected day.
insert_disorders - Set or update disorders for a selected day.
insert_sex - Set or update sex information for a selected day.
insert_sport - Set or update sports activity for a selected day.
insert_caprice - Set or update if you have eaten any whim for a selected day.
insert_alcohol - Set or update if you have drunk alcohol for a selected day.
insert_notes - Set or update the notes for a selected day.
get_info - Read the information available in period template for a selected date.
get_full_info - Returns image with temperature and data registry by date for current period.
get_graph_cycle - Returns a graph with temperatures by cycle day for current period.
get_graph_dates - Returns a graph with temperatures by calendar date for current period.
skip - Skip or EXIT from current activity/command.
start - Starts conversation with bot.
help - Print this commands list.