#!/bin/bash
# 
#   Docker run command example to start
#   TrackYourPeriod Bot polling.
#
#   Replace path of 'config.py' with your path.
#   It must be absolute path, don't use relative path.
#
#   Collaborators:
#       * Nil Torrano
#           <niltorrano@gmail.com>
#


docker run -d \
    --restart=always \
    -v YOUR_PATH/config.py:/credentials/config.py:ro \
    --name trackyourperiod zohantorrano/trackyourperiod:latest

exit 0
